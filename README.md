# Will the DVD screensaver hit the corner ?

Le but de ce projet était de reproduire le logo DVD qui rebondit et atteint les coins de l'écran.  
Le programme comprend des *statistiques* comme le temps depuis le lancement, le nombre de rebonds, le nombre de coin touchés.  
Les records sont *sauvegardés* dans un fichier

# Comment lancer le jeu ?

1. **téléchargez le projet**     `git clone gitlab.com/LBruneteau/dvd`  
2. Lancez le fichier *dvd_screensaver.py* avec python 3      `python dvd_screensaver.py`  
3. Appuyez sur `ESCAPE` pour quitter