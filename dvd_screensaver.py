#!/usr/bin/python3.8
from random import randrange, choice
import pickle
import sys
import os
import pygame

def temps():
    s = pygame.time.get_ticks() // 1000
    h = m = 0
    m, s = s // 60, s % 60
    h, m = m // 60, m % 60
    msg = f'{s}s'
    if bool(m):
        msg = f'{m}m ' + msg
    if bool(h):
        msg = f'{h}h ' + msg
    return msg 

def save():
    with open(chemin("save"), "wb") as fic:
        p = pickle.Pickler(fic)
        p.dump(record)

def chemin(relative_path):
    """ Get absolute path to resource, works for dev  for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

class DVD(pygame.Rect):

    def __init__(self):
        pygame.Rect.__init__(self, 0, 0, 100, 50)
        self.topleft = (randrange(2, 540, 2), randrange(2, 430, 2))
        self.image = pygame.image.load(chemin("logo_dvd.jpg"))
        self.image.convert_alpha()
        self.image.set_colorkey((0, 0, 0))
        self.image = pygame.transform.scale(self.image, (100, 50))
        self.vit_x = choice((-2, 2))
        self.vit_y = choice((-2, 2))

    def bouger(self):
        self.move_ip((self.vit_x, self.vit_y))

    def est_coin(self):
        return self.x in (0, 540) and self.y in (0, 430)

    def changer_vit(self, rebonds):
        if self.left == 0 or self.right == 640:
            self.vit_x *= -1
            rebonds += 1
        if self.top == 0 or self.bottom == 480:
            self.vit_y *= -1
            rebonds +=1
        return rebonds

    def aff(self, ecran):
        ecran.blit(self.image, self)

pygame.init()
blanc = (255, 255, 255)
ecran = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Will the DVD screensaver hit the corner ?")
pygame.display.set_icon(pygame.image.load(chemin("icone.ico")))
dvd = DVD()
h = pygame.time.Clock()
font = pygame.font.Font(chemin("Pixeled.ttf"), 12)
try:
    with open(chemin("save"), "rb") as fic:
        p = pickle.Unpickler(fic)
        record = p.load()
except:
    record = 0
pygame.register_quit(save)
score = 0
rebonds = 0
texte_rebond = font.render(f'REBONDS : {rebonds}', False, blanc)
texte_record = font.render(f'RECORD : {record}' ,False, blanc)
texte_score = font.render(f'SCORE : {score}', False, blanc)
texte_temps = font.render('0s', False, blanc)


boucle = True
while boucle:
    if dvd.est_coin():
        score += 1
        dvd = DVD()
        texte_score = font.render(f"SCORE : {score}", False, blanc)
        if score > record:
            record = score
            texte_record = font.render(f"RECORD : {record}", False, blanc)
    dvd.bouger()
    rebonds = dvd.changer_vit(rebonds)
    texte_rebond = font.render(f"REBONDS : {rebonds}", False, blanc)
    texte_temps = font.render(temps(), False, blanc)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            boucle = False
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            boucle = False
    ecran.fill((0, 0, 0))
    dvd.aff(ecran)
    ecran.blit(texte_record, (10, 375))
    ecran.blit(texte_score, (10, 395))
    ecran.blit(texte_rebond, (10, 415))
    ecran.blit(texte_temps, (10, 435))
    h.tick(50)
    pygame.display.flip()

